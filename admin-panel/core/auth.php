<?php

$admin_data = require 'config.php';

/* Проверка запроса */
if (!empty($_POST))
{
    /*Объеденяем массив в строку для большей безопасности хэша*/
    $post_string = implode('', $_POST);

    if (password_verify($post_string, $admin_data['admin_hash'])) {
        session_start();
        $_SESSION['admin'] = $admin_data['admin_hash'];
        echo 'Done';
    } else {
        echo 'False';
    }
}
