<?php
/* Проверка правильности данных*/

    function emailValidation($email)
    {
        $pattern = '/^[A-Za-z0-9\.\-_]+@[a-zA-Z0-9\-.]+\.[a-z]{2,4}$/';

        return preg_match($pattern, $email);
    }

    if(!emailValidation($_POST['user_email']))
    {
        echo json_encode(['error' => true, 'text' => 'Некорректный email. <br/> Пожалуйста, повторите попытку']);
        exit();
    }

/* Создаём подключение к базе данных */

    $config = require $_SERVER['DOCUMENT_ROOT'] . '/admin-panel/core/db_config.php';
    $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
    try {
        $pdo = new PDO($dsn, $config['user'], $config['password']);
    } catch (PDOException $e)
    {
        var_dump($e);
    }

/* Делаем запрос на добавление заявки в таблицу */

    $stm = $pdo->prepare("INSERT INTO applications (user_name, user_email, game_name, another_game_name, price, status)
                                    VALUES (:user_name, :user_email, :game_name, :another_game_name, :price, :status)");
    $stm->bindValue(':user_name', $_POST['user_name']);
    $stm->bindValue(':user_email', $_POST['user_email']);
    $stm->bindValue(':game_name', $_POST['game_name']);
    $stm->bindValue(':another_game_name', $_POST['another_game_name']);
    $stm->bindValue(':price', $_POST['price']);
    $stm->bindValue(':status', 'Открыта');

/* Отправка ответа на клиент */
    if($stm->execute())
    {
        echo json_encode(['error' => false, 'text' => 'Ваша заявка принята. <br/> Мы свяжемся с вами в ближайшее время']);
        $send = mail (
            $_POST['user_email'],
            'Заказ с сайта Steamkeys',
            'Ваша заявка на покупку ключа к игре ' . $_POST['game_name'] . ' успешно принята!',
            "Content-type:text/plain; charset = utf-8\r\nFrom:<steamkeys.ru>"
        );
    } else {
        echo json_encode(['error' => true, 'text' => 'Некорректные данные. <br/> Пожалуйста, повторите попытку']);
    }
