<?php

 /* Здесь проверяется существование переменных */
 if (isset($_POST))
 {
     $user_name = $_POST['user_name'];
     $user_email = $_POST['user_email'];
     $game_name = $_POST['game_name'];
     $message = $_POST['mail_message'];
 } else {
     echo json_encode(['error' => true, 'text' => 'Некорректные данные. <br/> Пожалуйста, повторите попытку']);
     exit();
 }

/* А эта функция как раз занимается отправкой письма на указанный вами email */
$sub='Заказ с сайта Steamkeys'; //сабж
$email='<steamkeys.ru>'; // от кого
$send = mail ($user_email, $sub, $message, "Content-type:text/plain; charset = utf-8\r\nFrom:$email");

/* Меняем статус заявки */

$config = require $_SERVER['DOCUMENT_ROOT'] . '/admin-panel/core/db_config.php';
$dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
try {
    $pdo = new PDO($dsn, $config['user'], $config['password']);
} catch (PDOException $e)
{
    var_dump($e);
}

$stmt = $pdo->prepare("UPDATE applications SET status = 'Выполнено' WHERE user_email = :user_email");
$stmt->bindValue(':user_email', $user_email);
$stmt->execute();

/* Отправляем письмо */
if($send) {
    echo json_encode(['error' => false, 'text' => 'Письмо отправлено!']);
} else {
    echo json_encode(['error' => true, 'text' => 'Что-то пошло не так. <br/> Пожалуйста, повторите попытку']);
}
