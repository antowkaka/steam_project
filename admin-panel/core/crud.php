<?php
if (isset($_POST))
{
    $config = require $_SERVER['DOCUMENT_ROOT'] . '/admin-panel/core/db_config.php';
    $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
    try {
        $pdo = new PDO($dsn, $config['user'], $config['password']);
    } catch (PDOException $e)
    {
        var_dump($e);
    }

    $stm = $pdo->prepare('SELECT * FROM games');
    $stm->execute();
    $games = $stm->fetchAll(PDO::FETCH_ASSOC);

    $stm = $pdo->prepare('SELECT * FROM keys');
    $stm->execute();
    $keys = $stm->fetchAll(PDO::FETCH_ASSOC);

    /* CREATE */
    if ($_POST['action'] === 'create')
    {
        if ($_POST['type'] === 'game') {

            foreach ($games as $key => $val)
            {
                if ($val['game_name'] === $_POST['game_name'])
                {
                    $game = [
                        'error_name' => 'уже существует в базе',
                        'error' => true
                    ];
                    echo json_encode(['error' => $game['error'], 'text' => 'Игра ' . $game['error_name']]);
                    break;
                }
            }

            if (!$game['error'])
            {
                $stmt = $pdo->prepare("INSERT INTO games (game_name, keys_count)
                                                VALUES (:game_name, :keys_count)");
                $stmt->bindValue(':game_name', $_POST['game_name']);
                $stmt->bindValue(':keys_count', 0);

                if ($stmt->execute())
                {
                    $game = [
                        'id' => count($games) + 1,
                        'name' => $_POST['game_name'],
                        'count' => 0,
                        'error' => false,
                        'error_name' => null
                    ];

                    echo json_encode(['error' => $game['error'], 'text' => 'Игра успешно добавлена!', 'data' => $game]);
                } else {
                    echo json_encode(['error' => true, 'text' => 'Ошибка добавления в БД']);
                }
            }

        } else if ($_POST['type'] === 'key')
        {
            $key = [
                'key_value',
                'game_name',
                'error',
                'error_name'
            ];

            if (preg_match('/^[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}$/', $_POST['key_value']))
            {
                if ($_POST['game_name']) {
                    $key['error'] = false;
                } else {
                    echo json_encode(['error' => true, 'text' => 'Выберите игру']);
                }
            } else {
                json_encode(['error' => true, 'text' => 'Некорректный ключ']);
            }

            if (!$key['error'])
            {
                $stmt = $pdo->prepare("INSERT INTO keys (key_value, game_name)
                                                VALUES (:game_name, :keys_count)");
                $stmt->bindValue(':game_name', $_POST['game_name']);
                $stmt->bindValue(':keys_count', 0);

                if ($stmt->execute())
                {
                    $game = [
                        'id' => count($games) + 1,
                        'name' => $_POST['game_name'],
                        'count' => 0,
                        'error' => false,
                        'error_name' => null
                    ];

                    echo json_encode(['error' => $game['error'], 'text' => 'Игра успешно добавлена!', 'data' => $game]);
                } else {
                    echo json_encode(['error' => true, 'text' => 'Ошибка добавления в БД']);
                }
            }
        }
    }

    /* READ*/


    /* UPDATE */


    /* DELETE */
}




