<?php
    $hash = require 'core/config.php';
    session_start();
    if($_SESSION['admin'] === $hash['admin_hash']){
        $config = require $_SERVER['DOCUMENT_ROOT'] . '/admin-panel/core/db_config.php';
        $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
        try {
            $pdo = new PDO($dsn, $config['user'], $config['password']);
        } catch (PDOException $e)
        {
            var_dump($e);
        }
    } else {
        header("Location:/admin-panel/index.php");
    }
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
</head>
<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                <i class="icon-reorder shaded"></i>
            </a>

            <a class="brand" href="pages/index.html">
                Админ панель
            </a>

            <div class="nav-collapse collapse navbar-inverse-collapse">

                <ul class="nav pull-right">
                    <li class="nav-user dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="images/user.png" class="nav-avatar" />
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="./core/logout.php">Выход</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.nav-collapse -->
        </div>
    </div><!-- /navbar-inner -->
</div><!-- /navbar -->



<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span3">
                <div class="sidebar">

                    <ul class="widget widget-menu unstyled">
                        <li class="active"><a href="main.php"><i class="menu-icon icon-dashboard"></i>Главная
                        </a></li>
                        <li><a href="applications.php"><i class="menu-icon icon-paste"></i>Заявки</a></li>
                        <li><a href="games.php"><i class="menu-icon icon-inbox"></i>Игры</a></li>
                        <li><a href="keys.php"><i class="menu-icon icon-key"></i>Ключи</a></li>
                        <li><a href="./core/logout.php"><i class="menu-icon icon-signout"></i>Выход</a></li>
                    </ul><!--/.widget-nav-->
                </div><!--/.sidebar-->
            </div><!--/.span3-->
            <?php
                $stm = $pdo->prepare('SELECT * FROM applications WHERE id = :id');
                $stm->bindParam(':id', $_GET['id']);
                $stm->execute();
                $application = $stm->fetch(PDO::FETCH_ASSOC);
                if($application['game_name'] === 'Выберите игру:')
                {
                    $application['game_name'] = $application['another_game_name'];
                }
            ?>
            <div class="span9">
                <div class="content">
                    <div class="module">
                        <div class="module-head">
                            <h3>Заявка №<?php echo $application['id']?></h3>
                        </div>
                        <div class="module-body">

                            <form class="form-horizontal row-fluid mail-form">

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Имя клиента</label>
                                    <div class="controls">
                                        <input type="text" id="basicinput"
                                               name="user_name"
                                               value="<?php echo $application['user_name']?>"
                                               placeholder="<?php echo $application['user_name']?>"
                                               class="span8" readonly>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Email клиента</label>
                                    <div class="controls">
                                        <input type="text" id="basicinput"
                                               name="user_email"
                                               value="<?php echo $application['user_email']?>"
                                               placeholder="<?php echo $application['user_email']?>"
                                               class="span8" readonly>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Выбранная игра</label>
                                    <div class="controls">
                                        <input type="text" id="basicinput"
                                               name="game_name"
                                               value="<?php echo $application['game_name']?>"
                                               placeholder="<?php echo $application['game_name']?>"
                                               class="span8" readonly>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <?php
                                        $stm = $pdo->prepare('SELECT game_key FROM games WHERE game_name = :name');
                                        $stm->bindParam(':name', $application['game_name']);
                                        $stm->execute();
                                        $key = $stm->fetch(PDO::FETCH_ASSOC);
                                    ?>
                                    <label class="control-label" for="basicinput">
                                        Текст сообщения:
                                    </label>
                                    <div class="controls">
                                        <textarea name="mail_message" class="span8" rows="5">Здравствуй, <?php echo $application['user_name']?> , спасибо за оплату! Вот твой ключ: <?php echo $key['game_key']?> от игры <?php echo $application['game_name']?>
                                        </textarea>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="controls">
                                        <button type="submit" class="btn btn-primary mail-submit">Отправить письмо</button>
                                    </div>
                                </div>

                                <br>

                                <div class="send-status alert-none">
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!--/.content-->
            </div><!--/.span9-->
        </div>
    </div><!--/.container-->
</div><!--/.wrapper-->

<div class="footer">
    <div class="container">
        <b class="copyright">&copy; 2020</b> All rights reserved.
    </div>
</div>

<script src="scripts/jquery-1.9.1.min.js"></script>
<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="scripts/datatables/jquery.dataTables.js"></script>
<script src="scripts/sendMail.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    } );
</script>
</body>