const applicationFieldLink = document.querySelectorAll('.app_table_field');
const gameFieldLink = document.querySelectorAll('.game_table_field');

applicationFieldLink.forEach(e => {
   e.addEventListener('click', () => {
       const applicationId = e.children[0].innerText;
       window.location.href = `/admin-panel/mail-form.php?id=${applicationId}`;
   })
});

gameFieldLink.forEach(e => {
    e.addEventListener('click', () => {
        window.location.href = '/admin-panel/games.php';
    })
});
