const mailSubmit = document.querySelector('.mail-submit');
const form = document.querySelector('.mail-form');
const mailStatusMessage = document.querySelector('.send-status');

mailSubmit.addEventListener('click', (e) => {
    e.preventDefault();
    let url = '/admin-panel/core/mailer.php';

    let oReq = new XMLHttpRequest();

    const formData = new FormData(form);
    oReq.open('POST', url);
    oReq.send(formData);

    oReq.onload = function () {
       let message = JSON.parse(oReq.responseText);
        mailStatusMessage.classList.remove('alert-none');
        if (message.error) {
            mailStatusMessage.classList.add('alert', 'alert-error');
        } else {
            mailStatusMessage.classList.add('alert', 'alert-success');
        }
        mailStatusMessage.innerHTML = message.text;
        console.log('DONE', oReq.readyState); // readyState будет равно 4
    };
});