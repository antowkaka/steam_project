const addButton = document.querySelector('.key-add-submit');
const addForm = document.querySelector('.key-add-form');
const addKeyStatus = document.querySelector('.add-game-status');


addButton.addEventListener('click', (e) => {
    e.preventDefault();
    let url = '/admin-panel/core/crud.php';

    let oReq = new XMLHttpRequest();

    const formData = new FormData(addForm);

    formData.append('action', 'create');
    formData.append('type', 'key');

    oReq.open('POST', url);
    oReq.send(formData);

    /*oReq.onload = function () {
        let message = JSON.parse(oReq.responseText);
        console.log(message);
        addGameStatus.classList.remove('alert-none');
        if (message.error) {
            addGameStatus.classList.add('alert', 'alert-error');
        } else {
            addGameStatus.classList.add('alert', 'alert-success');
        }
        addGameStatus.innerHTML = message.text;
        console.log('DONE', oReq.readyState); // readyState будет равно 4
    };*/
});