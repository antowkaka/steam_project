const authSubmit = document.querySelector('.auth-submit');
const form = document.querySelector('.auth-form');
const authMessage = document.querySelector('.auth-message');

authSubmit.addEventListener('click', (e) => {
    e.preventDefault();
    let url = '/admin-panel/core/auth.php';

    let oReq = new XMLHttpRequest();

    const formData = new FormData(form);
    oReq.open('POST', url);
    oReq.send(formData);

    oReq.onload = function () {
        let message = oReq.responseText;
        message === 'Done' ? window.location.href = '/admin-panel/main.php'
                           : authMessage.innerHTML = 'Непрвавильные данные. <br/> Попробуйте еще раз!';
        console.log('DONE', oReq.readyState); // readyState будет равно 4
    };
});