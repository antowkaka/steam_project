<?php
$hash = require 'core/config.php';
session_start();
if($_SESSION['admin'] === $hash['admin_hash']){
    $config = require $_SERVER['DOCUMENT_ROOT'] . '/admin-panel/core/db_config.php';
    $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'];
    try {
        $pdo = new PDO($dsn, $config['user'], $config['password']);
    } catch (PDOException $e)
    {
        var_dump($e);
    }
} else {
    header("Location:/admin-panel/index.php");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Keys</title>
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
</head>
<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                <i class="icon-reorder shaded"></i>
            </a>

            <a class="brand" href="pages/index.html">
                Админ панель
            </a>

            <div class="nav-collapse collapse navbar-inverse-collapse">

                <ul class="nav pull-right">
                    <li class="nav-user dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="images/user.png" class="nav-avatar" />
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="./core/logout.php">Выход</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.nav-collapse -->
        </div>
    </div><!-- /navbar-inner -->
</div><!-- /navbar -->



<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span3">
                <div class="sidebar">

                    <ul class="widget widget-menu unstyled">
                        <li class="active"><a href="main.php"><i class="menu-icon icon-dashboard"></i>Главная
                            </a></li>
                        <li><a href="applications.php"><i class="menu-icon icon-paste"></i>Заявки</a></li>
                        <li><a href="games.php"><i class="menu-icon icon-inbox"></i>Игры</a></li>
                        <li><a href="keys.php"><i class="menu-icon icon-key"></i>Ключи</a></li>
                        <li><a href="./core/logout.php"><i class="menu-icon icon-signout"></i>Выход</a></li>
                    </ul><!--/.widget-nav-->
                </div><!--/.sidebar-->
            </div><!--/.span3-->


            <div class="span9">
                <div class="module">
                    <div class="module-head">
                        <h3>Добавить ключ</h3>
                    </div>
                    <div class="module-body">

                        <form class="form-inline row-fluid key-add-form">

                            <div class="control-group">
                                <div class="controls">
                                    <input type="text" id="basicinput"
                                           name="key_value"
                                           placeholder="Значение ключа"
                                           class="span8">
                                    <button type="submit" class="btn btn-primary key-add-submit">Добавить</button>
                                </div>
                                <br/>
                                <div class="controls">
                                    <select name="game_name" tabindex="1" data-placeholder="Select here.." class="span8">
                                        <option value="">Выберите игру..</option>
                                        <?php
                                        $stm = $pdo->prepare('SELECT id, game_name FROM games');
                                        $stm->execute();
                                        $keys = $stm->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($keys as $k => $v) {
                                            ?>
                                            <option value="<?php echo $v['game_name']?>"
                                                    data-id="<?php echo $v['id']?>">
                                                    <?php echo $v['game_name']?>
                                            </option>
                                        <?php }; ?>
                                    </select>
                                </div>
                                <br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input type="text" id="basicinput"
                                               name="key_value"
                                               placeholder="Значение ключа"
                                               class="span8">
                                        <button type="submit" class="btn btn-primary key-add-submit">Добавить</button>
                                    </div>
                                </div>
                            </div>
                            <div class="add-game-status alert-none">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="content">
                    <div class="module">
                        <div class="module-head">
                            <h3>Ключи</h3>
                        </div>
                        <div class="module-body table">
                            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Ключ</th>
                                    <th>Название игры</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $stm = $pdo->prepare('SELECT * FROM keys');
                                $stm->execute();
                                $keys = $stm->fetchAll(PDO::FETCH_ASSOC);
                                foreach ($keys as $k => $v) {
                                    ?>
                                    <tr>
                                        <td><?php echo $v['id']?></td>
                                        <td><?php echo $v['key_value']?></td>
                                        <td><?php echo $v['game_name']?></td>
                                    </tr>
                                <?php }; ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Ключ</th>
                                    <th>Название игры</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div><!--/.module-->

                    <br />

                </div><!--/.content-->
            </div><!--/.span9-->
        </div>
    </div><!--/.container-->
</div><!--/.wrapper-->

<div class="footer">
    <div class="container">


        <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b> All rights reserved.
    </div>
</div>

<script src="scripts/jquery-1.9.1.min.js"></script>
<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="scripts/datatables/jquery.dataTables.js"></script>
<script src="scripts/addKey.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    } );
</script>
</body>