const submitBnt = document.querySelector('.btn-submit');
const form = document.querySelector('.application-form');
const submitMessage = document.querySelector('.submit-message');

submitBnt.addEventListener('click', (e) => {
    e.preventDefault();
    let url = '/admin-panel/core/send.php';

    let oReq = new XMLHttpRequest();

    const formData = new FormData(form);
    oReq.open('POST', url);
    oReq.send(formData);

    oReq.onload = function () {
        let message = JSON.parse(oReq.responseText);

        submitMessage.innerHTML = message.text;
        message.error ? submitMessage.style.backgroundColor = '#FD9743' : submitMessage.style.backgroundColor = '#299699';
        submitMessage.style.display = 'flex';
        console.log(message);
        console.log('DONE', oReq.readyState); // readyState будет равно 4
    };
});
