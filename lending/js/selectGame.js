const opts = document.querySelectorAll('.opt');
const gameButtons = document.querySelectorAll('.game-pic');
const formSection = document.querySelector('#section4');
const others = document.querySelector('.others-input');
const price = document.querySelector('.price-input');


gameButtons.forEach(btn => {
   btn.addEventListener('click', () => {
        opts.forEach(opt => {
            if (btn.dataset.gamename === 'Others') {
                others.focus();
                price.value = 0;
            } else {
                price.value = btn.children[1].innerHTML.slice(0, -1);
                btn.dataset.gamename === opt.value ? opt.selected = true : null;
            }
        });
        formSection.scrollIntoView({behavior: "smooth"});
   })
});