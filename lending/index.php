<?php if(isset($_GET['fbclid'])) {
    $title_src = 'img/logo-text-black.png';
    $game_prices = [
        'RDR2' => '10$',
        'Resident Evil 2' => '8$',
        'Control' => '10$',
        'PUBG' => '5$',
        'Witcher 3' => '6$',
    ];
} else {
    $title_src = 'img/logo-text.png';
    $game_prices = [
        'RDR2' => '20$',
        'Resident Evil 2' => '16$',
        'Control' => '20$',
        'PUBG' => '10$',
        'Witcher 3' => '12$',
    ];
}
?>

<!DOCTYPE html>
<!--[if lte IE 9]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Steamkeys</title>
    <!--=================================
    Meta tags
    =================================-->
    <meta name="description" content="">
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no"/>
    <!--=================================
    Style Sheets
    =================================-->
    <link href='http://fonts.googleapis.com/css?family=Cabin:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/animations.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.flickr.css">
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
    <link rel="stylesheet" href="css/main.css">

    <script async src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body class="eschool" data-spy="scroll" data-target="#navbar" data-offset="70">

<!--====================================
Body Content
=======================================-->
<div class="navbar-custom">
    <div class="container">
            <a href="index.html" class="logo"><img src="<?php echo $title_src ?>" alt="steamkeys"></a>
        <nav id="navbar">
            <a href="#" class="nav-triger"><span class="fa fa-navicon"></span></a>
            <ul class="main-menu nav">
                <li><a href="#section0">Вступление</a></li>
                <li><a href="#section1">О нас</a></li>
                <li><a href="#section2">Наши игры</a></li>
                <li><a href="#section3">Отзывы</a></li>
                <li><a href="#section4">Оставить заявку</a></li>
            </ul>
        </nav>

    </div>
</div>

<section id="section0" class="header">
    <div class="container main-logo">
        <a href="index.html"><img src="img/logo-test-main.png" alt="Main-logo"></a>
    </div>
</section>

<section id="section1" class="our-courses mt-30">
    <div class="container">
        <div class="text-center mb-50 head">
            <h2 class="mb-25">О <span>НАШЕМ</span> проекте</h2>
            <p>Steam ключи купить дешево, по самой низкой цене, можно только в нашем магазине steam ключей.
                Основные плюсы покупки steam игры у нас, это очень низкая цена(дешевле нигде нет) и высокая скорость доставки.
                Также покупая у нас, вы получаете надежную поддержку в течение долгих лет. Большинство ключей закупается у издателей напрямую, что делает их дешевле, чем у конкурентов.</p>
        </div>
        <div class="row courses top">
            <div class="col-sm-4 course text-bold clearfix">
                <figure class="pull-left mt-10"><i style="font-size:80px; color:#0604f3" class="fa fa-check-circle"
                                                   aria-hidden="true"></i></figure>
                <div class="info">
                    <h5>Преимущество №1</h5>
                    <p>Самая низкая цена среди конкурентов.</p>
                </div>
            </div>
            <div class="col-sm-4 course text-bold clearfix">
                <figure class="pull-left mt-10"><i style="font-size:80px; color:#FD9743" class="fa fa-check-circle"
                                                   aria-hidden="true"></i></figure>
                <div class="info">
                    <h5>Преимущество №2</h5>
                    <p>Скорость обработки заявок. Оставьте заявку и наш менеджер с вами свжеться в течении 10 минут.</p>
                </div>
            </div>
            <div class="col-sm-4 course text-bold clearfix">
                <figure class="pull-left mt-10"><i style="font-size:80px; color:#f32121" class="fa fa-check-circle"
                                                   aria-hidden="true"></i></figure>
                <div class="info">
                    <h5>Преимущество №3</h5>
                    <p>Легкость активации ключей. Наш менеджер скинет вам инструкцию с активацией в удобный для вас мессенджер.</p>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="section2" class="our-courses parallax parallax_one" data-stellar-background-ratio="0.5"
         style="background-position: center bottom">
    <div class="overlay"></div>
    <div class="parallax_inner">
        <div class="container">
            <div class="text-center mb-50 head">
                <h2 class="mb-25" style="color:#fff">Топ <span style="color:#FD9743">игр</span></h2>
            </div>
            <div class="row courses">
                <div class="col-md-4 col-sm-6 course text-bold clearfix">
                    <div class="course_inner">
                        <figure class="pull-left mt-10 game-pic" data-gamename="Red Dead Redemption 2">
                            <img class="game-pic-img" alt="" style="height:300px;" src="img/rdr-picture.jpg">
                            <span class="game-pic-price"><?php echo $game_prices['RDR2']; ?></span>
                        </figure>
                        <div>
                            <h5 class="game-name">Dead Redemption 2</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 course text-bold clearfix">
                    <div class="course_inner">
                        <figure class="pull-left mt-10 game-pic" data-gamename="Resident Evil 2">
                            <img class="game-pic-img" alt="" style="height:300px;" src="img/resident-evil-picture.jpeg">
                            <span class="game-pic-price"><?php echo $game_prices['Resident Evil 2']; ?></span>
                        </figure>
                        <div>
                            <h5 class="game-name">Resident Evil 2</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 course text-bold clearfix">
                    <div class="course_inner">
                        <figure class="pull-left mt-10 game-pic" data-gamename="Control">
                            <img class="game-pic-img" alt="" style="height:300px;" src="img/control-picture.jpg">
                            <span class="game-pic-price"><?php echo $game_prices['Control']; ?></span>
                        </figure>
                        <div>
                            <h5 class="game-name">Control</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 course text-bold clearfix">
                    <div class="course_inner">
                        <figure class="pull-left mt-10 game-pic" data-gamename="PUBG">
                            <img class="game-pic-img" alt="" style="height:300px;" src="img/pubg-picture.jpg">
                            <span class="game-pic-price"><?php echo $game_prices['PUBG']; ?></span>
                        </figure>
                        <div>
                            <h5 class="game-name">Playrunknown`s battleground</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 course text-bold clearfix">
                    <div class="course_inner">
                        <figure class="pull-left mt-10 game-pic" data-gamename="Witcher 3">
                            <img class="game-pic-img" alt="" style="height:300px;" src="img/witcher-picture.jpg">
                            <span class="game-pic-price"><?php echo $game_prices['Witcher 3']; ?></span>
                        </figure>
                        <div>
                            <h5 class="game-name">Witcher 3</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 course text-bold clearfix">
                    <div class="course_inner">
                        <figure class="pull-left mt-10 game-pic" data-gamename="Others">
                            <img class="game-pic-img" alt="" style="height:300px;" src="img/other_games.jpg"></figure>
                        <div>
                            <h5 class="game-name">Другие игры</h5>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<section id="section3" class="testimonials text-bold text-center color-white parallax"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="parallax_inner owl-carousel" data-slides="1" data-slides-md="1" data-slides-sm="1" data-margin="0"
         data-loop="true" data-dots="true" data-nav="false">
        <div class="container">
            <figure><img alt="" src="img/testimonial.png"></figure>
            <h5>John Snow</h5>
            <p>Действительно, моментальное получение ключа (рабочего, смею заметить), приятная цена. Спасибо!</p>
        </div>
        <div class="container">
            <figure><img alt="" src="img/testimonial-joker.jpg"></figure>
            <h5>Mr. Joker</h5>
            <p>Огромное спасибо! Первый раз покупал на этом сайте, всё быстро, чётко Оплачивал через Приват, там была комиссия в районе 5 грн., но это фигня!</p>
        </div>
        <div class="container">
            <figure><img alt="" src="img/testimonial1.jpg"></figure>
            <h5>Просто Мейсон</h5>
            <p>Ключ сразу заработал. Быстро и оперативно получил его. Отличный продавец, здоровья ему, желаю ему и всем его близким всего наилучшего, хорошего настроения и здоровья!</p>
        </div>
    </div>
</section>

<section id="section4" class="our-courses parallax parallax_one" data-stellar-background-ratio="0.5"
         style="background-position: center bottom">
    <div class="overlay"></div>
    <div class="parallax_inner">
        <div class="container">
            <div class="form-title main-form">
                <h2>Выбирай игру и оставляй заявку</h2>
                <div class="row mt-30 main-form">
                    <div class="col-md-6 col-sm-6">
                        <form class="application-form">
                            <input name="user_name" type="text" placeholder="Ваше Имя:">
                            <input name="user_email" type="email" placeholder="Ваш Email:">
                            <select name="game_name" class="form-control game-selector">
                                <option class="opt" selected>Выберите игру:</option>
                                <option class="opt" value="Red Dead Redemption 2">Red Dead Redemption 2</option>
                                <option class="opt" value="Resident Evil 2">Resident Evil 2</option>
                                <option class="opt" value="Control">Control</option>
                                <option class="opt" value="PUBG">PUBG</option>
                                <option class="opt" value="Witcher 3">Witcher 3</option>
                            </select>
                            <input name="another_game_name" class="others-input" type="text" placeholder="Другая игра:">
                            <input name="price" class="price-input" type="hidden">
                            <button class="btn btn-default btn-head btn-submit mt-20">Оставить заявку</button>
                            <div class="submit-message mt-20 mb-50"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<footer class="text-center color-white text-bold">
    <span>© Steamkeys, 2020</span>
</footer>
<!--===============================
Script Source
=================================-->

<script src="js/jquery.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.easing-1.3.pack.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/bootstrap-scrollspy.min.js"></script>
<script src="js/selectGame.js"></script>
<script src="js/send-form.js"></script>
<script src="js/main.js"></script>

</body>
</html>
